# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.2] - 2019-11-21
### Fixed
- Recaptcha not working due to whitespace stripping

## [1.5.1] - 2019-11-5
### Fixed
- Final add to cart on product page will now add the correct product
- Custom select dropdown will no appear using the correct colours
- `<br>` within hero banner text are now hidden on mobile for native text wrapping

## [1.5.0] - 2019-11-4
### Added
- Add custom setting to link the terms and conditions page (fixes broken checkout link), this has a fallback of the shop terms of service policy url

## [1.4.4] - 2019-11-1
### Fixed
- `requires_age_check` - is now correctly assigned within the `product-card.liquid` template

## [1.4.3] - 2019-10-30
### Fixed
- Add to cart on product-card
- Small mobile text overflow issues

## [1.4.2] - 2019-10-30
### Fixed
- User settings will now correctly redirect to the country store
- Removes duplicate currencies from dropdown

## [1.4.1] - 2019-10-30
### Fixed
- When automatically redirecting to local store, it will now set the correct store currency
- Dynamically populate user setting dropdowns to be consistent across all stores.

## [1.4.0] - 2019-10-29
### Added
- US google tag manager / remarketing scripts

## [1.3.1] - 2019-10-24
### Fixed
- Wrong import causing loss of functionality

## [1.3.0] - 2019-10-24
### Changed
- Allows teaser content title to act like the statement

## [1.2.0] - 2019-10-24
### Added
- Auto redirect with configurable settings
- Configurable hero banner settings

## [1.1.5] - 2019-10-24
### Fixed
- Add to cart on the homepage
- FAQs and modal bug on product page

## [1.1.4] - 2019-10-24
### Added
- Breadcrumbs added across all sites
- US custom logo

## [1.1.3] - 2019-10-24
### Fixed
- Shop dropdown will use the collection nav for all stores except US. The US will use the default navigation.

## [1.1.2] - 2019-10-24
### Changed
- Shift the product teaser content below the how to / nutritional / benefits

## [1.1.1] - 2019-10-24
### Fixed
- Fixes dropdown showing for non US stores without links

## [1.1.0] - 2019-10-24
### Added
- Allows blog to have a hero banner
- New add to cart on product card

### Changed
- Remove shop dropdown except US 🤷‍♂️
- Remove blacklisted states for US

## [1.0.2] - 2019-10-22
### Changed
- Founder images are now optional

## [1.0.1] - 2019-10-22
### Fixed
- Replaces hard coded text with translatable string
- Fixes `CartPreview` displaying GBP for all countries

### Changed
- US S&S: Hides nutritional tab in product information modal

## [1.0.0] - 2019-08-15
- Initial tag release
