/**
 * @prettier
 */
const path = require('path');
const webpack = require('webpack');
const FlowBabelWebpackPlugin = require('flow-babel-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: {
        main: path.resolve(__dirname, 'src/js/main.js'),
        critical: path.resolve(__dirname, 'src/js/critical.js'),
        pollen: path.resolve(__dirname, 'src/js/pollen.js'),
        'smith-and-sinclair': path.resolve(__dirname, 'src/js/smith-and-sinclair.js'),
        carousel: path.resolve(__dirname, 'src/js/carousel.js')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextWebpackPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: () => [
                                    require('autoprefixer')(),
                                    require('css-mqpacker')()
                                ]
                            }
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
            {
                test: /\.(woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        context: '',
                        name: '[name].[ext]',
                        outputPath: './',
                        publicPath: './'
                    }
                }
            }
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        }
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'src/assets/'),
        publicPath: '/assets/'
    },
    plugins: [
        new Dotenv(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        }),
        // new FlowBabelWebpackPlugin(),
        new ExtractTextWebpackPlugin({
            filename: '../assets/[name].css'
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'src/img/'),
                to: path.resolve(__dirname, 'src/assets/')
            }
        ])
    ],
    resolve: {
        extensions: ['.js'],
        modules: ['node_modules', path.resolve(__dirname, 'src')]
    }
};
