/*
 * @flow
 */
export const SPACING = 24;
export const SPACING_SMALL = 12;
export const SPACING_TINY = 8;

export const ANIMATION_DURATION = 600;
export const ANIMATION_EASING = 'easeOutExpo';
export const ANIMATION_ELASTICITY = ANIMATION_DURATION / 3;

// TODO: Change this config for a sanitised object based collection of stores with helper methods to find them
// export const STORES = [
//      {
//          name: 'Germany',
//          language: 'German',
//          currency: 'EUR',
//          url: 'https://de.smithandsinclair.com/'
//      }
// ]

export const CURRENCIES = {
    EUR: 'Euros',
    GBP: 'Pounds',
    USD: 'Dollars'
}

export const LANGUAGES = {
    DE: 'German',
    EN: 'English',
    ES: 'Spanish',
    FR: 'French',
    IT: 'Italian',
    NL: 'Dutch'
}

export const STORE_LOCAL_CURRENCIES = {
    pollen: {
        // TODO: Fill this out
    },

    'smith-and-sinclair': {
        DE: 'EUR',
        ES: 'EUR',
        FR: 'EUR',
        IT: 'EUR',
        GB: 'GBP',
        NL: 'EUR',
        US: 'USD',
    }
}

export const COUNTRIES = {
    pollen: {
        // TODO: Fill this out
    },
    'smith-and-sinclair': {
        DE: "Germany",
        ES: "Spain",
        FR: "France",
        IT: "Italy",
        NL: "The Netherlands",
        GB: "United Kingdom",
        US: "United States"
    }
}


export const STORE_URLS = {
    pollen: {
        // TODO: Fill these out
    },
    'smith-and-sinclair': {
        GB: "https://www.smithandsinclair.com/",
        IT: "https://it.smithandsinclair.com/",
        ES: "https://es.smithandsinclair.com/",
        FR: "https://fr.smithandsinclair.com/",
        DE: "https://de.smithandsinclair.com/",
        NL: "https://nl.smithandsinclair.com/",
        US: "https://us.smithandsinclair.com/"
    }
}
