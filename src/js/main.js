/*
 * @flow
 */
import hasJS from './components/has-js';
import cartPreview from './components/cart-preview';
import marquee from './components/marquee';
import {init as modalInit} from './components/modal';
import overlayNavigation from './components/overlay-navigation';
import scrollMonitor from './components/scroll-monitor';
import sticky from './components/sticky';
import tabList from './components/tab-list';
import tracking from './components/tracking';
import userSettings from './components/user-settings';
import visibilityToggle from './components/visibility-toggle';
import products from './product';
import '../sass/main.scss';

const init = () => {
    hasJS.init();
    tracking.init();
    modalInit();
    userSettings.init();
    sticky.init();
    overlayNavigation.init();
    scrollMonitor.init();
    visibilityToggle.init();
    cartPreview.init();
    marquee.init();
    tabList.init();
    products.init();
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
