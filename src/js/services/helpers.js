/*
 * @flow
 */
import round from 'lodash/round';
import get from 'lodash/get';

const currencies = get(window, 'globalData.currencies');

/**
 * Still figuring out Flow types - will come back to this!
 * - For recursive functions, how do we define return types for a specific value or the function?
 * - How can HTMLElement types reference their 'possibly null' parentNode values?
 */
export const intendedTargetElement = (
    selector: string,
    element: Object
): HTMLElement | intendedTargetElement => {
    if (element && element.classList && element.classList.contains(selector) === false) {
        if (element.parentNode) {
            return intendedTargetElement(selector, element.parentNode);
        }
    }

    return element;
};

export const getFetchParams = (data?: Object, method: string = 'get'): Object => {
    let params = {
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'},
        method
    };

    if (data) {
        params = {
            ...params,
            method: 'post',
            body: JSON.stringify(data)
        };
    }

    return params;
};

/**
 * Copied from Shopify's Slate tools.
 */
const formatWithDelimiters = (number = 0, precision = 2, thousands = ',', decimal = '.') => {
    if (isNaN(number) || number == null) {
        return 0;
    }

    number = (parseInt(number) / 100.0).toFixed(precision);

    const parts = number.split('.');
    const dollarsAmount = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands);
    const centsAmount = parts[1] && parts[1] !== '00' ? decimal + parts[1] : '';

    return dollarsAmount + centsAmount;
};

/**
 * Copied from Shopify's Slate tools.
 */
export const formatMoney = (cents, format) => {
    if (typeof cents === 'string') {
        cents = cents.replace('.', '');
    }

    const moneyFormat = '£{{amount}} GBP';
    const placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;
    const formatString = format || moneyFormat;
    const match = formatString.match(placeholderRegex);
    let value = '';

    if (match && match.length >= 2) {
        switch (match[1]) {
            case 'amount':
                value = formatWithDelimiters(cents, 2);
                break;
            case 'amount_no_decimals':
                value = formatWithDelimiters(cents, 0);
                break;
            case 'amount_with_comma_separator':
                value = formatWithDelimiters(cents, 2, '.', ',');
                break;
            case 'amount_no_decimals_with_comma_separator':
                value = formatWithDelimiters(cents, 0, '.', ',');
                break;
            default:
                value = formatWithDelimiters(cents, 2);
                break;
        }

        if (currencies.roundPrice) {
            value = Math.round(parseFloat(value));
        }

        return formatString.replace(placeholderRegex, value.toString());
    }
};

// @deprecated use formatMoney
export const getPrice = (value: number, currency: string = '£', roundBy: number = 100): string => {
    return `${currency}${round(value / roundBy, 2)}`;
};

export const preloadImages = (images: Array<string> = []): void => {
    images.map(imageURL => {
        if (typeof Image !== undefined) {
            let image = new Image();

            image.src = imageURL;
        }
    });
};

export const handleFetchErrors = (response: Object) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
};

const isTabletDevice = () =>
    navigator.userAgent.match(/Tablet|iPad|iPod/i) &&
    window.innerWidth <= 1280 &&
    window.innerHeight >= 800;

export const isMobile = () => {
    if (typeof window !== 'undefined') {
        const layout = window
            .getComputedStyle(document.querySelector('body'), ':before')
            .getPropertyValue('content');

        return layout.includes('mobile') && !isTabletDevice();
    }

    return false;
};

export const getOrdinal = (i: number) => {
    const j = i % 10;
    const k = i % 100;

    if (j == 1 && k != 11) {
        return `${i}st`;
    }

    if (j == 2 && k != 12) {
        return `${i}nd`;
    }

    if (j == 3 && k != 13) {
        return `${i}rd`;
    }

    return `${i}th`;
};

export const updateHistory = (path: string) => {
    if (window && history.pushState) {
        const {protocol, host, pathname} = window.location;
        const URL = `${protocol}//${host}${path}`;

        window.history.pushState({path: URL}, '', URL);
    }
};

export const getAddAllToCartURL = (variantIds: Array<number>) =>
    `/cart/add?${variantIds.map(id => `id[]=${id}`).join('&')}`;

export const getUrlParams = (params: Object = {}) => {
    const paramsString = Object.keys(params)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
        .join('&');

    return paramsString;
};

export const isDevelopmentSite = () => {
    return ['localhost', 'smithandsinclair-development.myshopify.com'].includes(window.location.hostname)
}
