/*
 * @flow
 */
import throttle from 'lodash/throttle';
import Rellax from 'rellax';

let $parallaxEls;
let rellax;

const setupParallax = () => {
    if ($parallaxEls.length) {
        if (rellax) {
            rellax.destroy();
        }

        rellax = new Rellax('.u-parallax');
    }
};

const addEvents = () => {
    window.addEventListener('resize', throttle(setupParallax, 300));
};

const cacheElements = () => {
    $parallaxEls = ((document.querySelectorAll('.u-parallax'): any): NodeList<HTMLElement>);
};

const init = () => {
    cacheElements();
    addEvents();
    setupParallax();
};

export default {
    init
};
