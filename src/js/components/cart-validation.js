/*
 * @flow
 */
import get from 'lodash/get';
import Cookie from 'js-cookie';
import {getFetchParams} from '../services/helpers';

const cartData = get(window, 'globalData.cart');

let $checkoutButton;

const validateAgeCheckedProducts = () => {
    const userSettings = Cookie.getJSON('userSettings');

    if (userSettings && userSettings.ageConfirmed === userSettings.ageRequired) {
        return;
    }

    if (cartData && cartData.items) {
        const products = Promise.all(
            cartData.items.map(item => {
                return fetch(`/products/${item.handle}.js`, getFetchParams()).then(r => r.json());
            })
        );

        products.then(responses => {
            const ageCheckedProducts = responses.filter(response =>
                response.tags.includes('age_check')
            );
        });
    }
};

const cacheElements = () => {
    $checkoutButton = ((document.querySelectorAll('.js-cart-count'): any): HTMLButtonElement);
};

const init = () => {
    cacheElements();
    validateAgeCheckedProducts();
};

export default {init};
