/*
 * @flow
 */
import {h, render} from 'preact';
import React from 'preact/compat';
import find from 'lodash/find';
import get from 'lodash/get';
import pickBy from 'lodash/pickBy';
import Cookie from 'js-cookie';
import 'formdata-polyfill';
import 'url-search-params-polyfill';
import currencyFormats from './currency-formats';
import {formatMoney, isDevelopmentSite} from './../services/helpers'
import {closeModal, init as modalInit, openModal, setActiveModal} from './modal';
import {STORE_URLS, STORE_LOCAL_CURRENCIES, COUNTRIES, CURRENCIES, LANGUAGES} from '../config';


type Settings = {
    ageConfirmed?: boolean,
    ageMinimum: number,
    ageRequired: boolean,
    brand: String,
    countries: Object,
    country: string,
    countryConfirmed?: boolean,
    currency: string,
    currencies: Array<string>,
    flags: Object,
    language: string,
    languages: Array<string>,
    storeUrls: Object
};

const SHOPIFY_COOKIE_NAME = 'cart_currency';
const COUNTRY_LANG_CURRENCY_RECOMMENDATIONS = [
    {country: 'DE', currency: 'EUR', language: 'DE'},
    {country: 'ES', currency: 'EUR', language: 'ES'},
    {country: 'FR', currency: 'EUR', language: 'FR'},
    {country: 'GB', currency: 'GBP', language: 'EN'},
    {country: 'IT', currency: 'EUR', language: 'IT'},
    {country: 'NL', currency: 'EUR', language: 'NL'},
    {country: 'US', currency: 'USD', language: 'EN'}
];

const shopifyCurrency = Cookie.get(SHOPIFY_COOKIE_NAME);
let brand = get(window, 'globalData.storeSettings.brand', 'smith-and-sinclair')

let userSettings: Settings | null = Cookie.getJSON('userSettings');
let storeSettings: Settings = {
    ageMinimum: 18,
    ageRequired: true,
    country: 'GB',
    countries: COUNTRIES[brand],
    currency: 'GBP',
    currencies: CURRENCIES,
    languages: LANGUAGES,
    storeUrls: STORE_URLS[brand],
    ...get(window, 'globalData.storeSettings', {})
};

let $ageConfirmForm;
let $ageContainer;
let $ageFallback;
let $ageMinimum;
let $countrySelector;
let $currencySelector;
let $ctaButton;
let $ctaButtonText;
let $flagImage;
let $prices;
let $recommendationContainer;
let $recommendationConfirmButtons;
let $recommendationText;
let $settingsContainer;
let $settingsForm;
let $stateContainer;
let $stateSelector;


const convertAllPrices = newCurrency => {
    if ($prices) {
        const defaultCurrency = shopifyCurrency || 'GBP';

        [...$prices].map($price => {
            if (newCurrency !== defaultCurrency) {
                const newPrice = window.Currency.convert(
                    parseFloat($price.dataset.defaultPrice),
                    defaultCurrency,
                    newCurrency
                );

                if (currencyFormats[newCurrency]) {
                    const formattedPrice = formatMoney(
                        newPrice,
                        currencyFormats[newCurrency].money_format
                    );

                    if (formattedPrice) {
                        $price.innerHTML = formattedPrice;
                        $price.setAttribute('data-currency', newCurrency);
                        $price.setAttribute('data-price', `${newPrice}`);
                    }
                }
            } else {
                $price.innerHTML = $price.dataset.defaultPriceWithCurrency;
                $price.setAttribute('data-currency', $price.dataset.defaultCurrency);
                $price.setAttribute('data-price', $price.dataset.price);
            }
        });
    }
};

const setDefaultFormValues = () => {
    const savedSettings = Cookie.getJSON('userSettings');

    if ($settingsForm && savedSettings && $countrySelector && $currencySelector) {
        $countrySelector.value = savedSettings.country;
        $currencySelector.value = savedSettings.currency;
    }
};

const closeSettingsModal = () => {
    closeModal();
};

const openSettingsModal = () => {
    setDefaultFormValues();
    setActiveModal('user-settings');
    openModal(true);
};

const updateHeaderSettingsCTA = () => {
    if (userSettings) {
        if ($ctaButtonText) {
            [...$ctaButtonText].map($el => ($el.innerText = userSettings.currency));
        }

        if ($flagImage && storeSettings) {
            [...$flagImage].map($el => {
                $el.src = storeSettings.flags[userSettings.country];
            });
        }
    }
};

const updateAgeRequirementFields = age => {
    if (userSettings) {
        if ($ageMinimum && (age || userSettings.ageMinimum)) {
            $ageMinimum.innerText = `${age || userSettings.ageMinimum}`;
        }
    }
};

const requestUserSettingsConfirmation = (type = 'settings') => {
    if (userSettings && $ageContainer && $settingsContainer && $stateContainer) {
        if (type === 'age') {
            $ageContainer.classList.remove('u-hide');
            $settingsContainer.classList.add('u-hide');
            $stateContainer.classList.toggle(
                'u-hide',
                userSettings.country !== 'US' || storeSettings.country !== 'US'
            );
        } else {
            $ageContainer.classList.add('u-hide');
            $settingsContainer.classList.remove('u-hide');
            $stateContainer.classList.add('u-hide');
        }

        if ($recommendationContainer) {
            $recommendationContainer.classList.add('u-hide');
        }

        openSettingsModal();
    }
};
window.requestUserSettingsConfirmation = requestUserSettingsConfirmation;

const handleSettingsModalClick = (event: UIEvent) => requestUserSettingsConfirmation();

const attemptRedirectToLocalStore = () => {
    if (
        userSettings &&
        userSettings.country &&
        storeSettings &&
        storeSettings.storeUrls[userSettings.country] &&
        typeof window !== 'undefined'
    ) {
        const pickedSettings = pickBy(userSettings, (v, k) =>
            ['ageConfirmed', 'currency', 'language', 'country', 'countryConfirmed'].includes(k)
        );
        const params = encodeURIComponent(JSON.stringify(pickedSettings));
        const newStoreUrl = `${storeSettings.storeUrls[userSettings.country]}?_us=${params}`;

        if (isDevelopmentSite()) {
            console.log('Would redirect to:', newStoreUrl, pickedSettings);
        } else {
            window.location = newStoreUrl;
        }
    }
};

const updateUserSettings = (newSettings: Object = {}, shouldRedirectToStore: Boolean = false) => {
    const prevSettings = userSettings;

    userSettings = {...userSettings, ...newSettings};
    Cookie.set('userSettings', userSettings);

    updateHeaderSettingsCTA();
    updateAgeRequirementFields();

    if (prevSettings) {
        if (
            shouldRedirectToStore &&
            (prevSettings.country && userSettings.country !== prevSettings.country) ||
            (!prevSettings.country && userSettings.country !== storeSettings.country)
        ) {
            attemptRedirectToLocalStore();
        }

        if (prevSettings.currency !== userSettings.currency) {
            convertAllPrices(userSettings.currency);
        }
    }
};

const showRecommendations = (userSettings, recommendedSettings) => {
    if (!$recommendationContainer || !$recommendationText || !$recommendationConfirmButtons) {
        $recommendationContainer = ((document.querySelector(
            '.js-user-recommendation-container'
        ): any): HTMLDivElement);
        $recommendationText = ((document.querySelector(
            '.js-recommendation-text'
        ): any): HTMLSpanElement);
        $recommendationConfirmButtons = ((document.querySelectorAll(
            '.js-recommendation-confirm'
        ): any): NodeList<HTMLButtonElement>);
    }

    if (
        $settingsContainer &&
        $recommendationContainer &&
        $recommendationText &&
        $recommendationConfirmButtons
    ) {
        const currency = userSettings.currency !== recommendedSettings.currency;

        let text = `You have selected our ${
            storeSettings.countries[userSettings.country]
            } store, but we have detected you are `;

        // if (currency) {
        //     text = `${text} view prices with ${storeSettings.currencies[userSettings.currency]}`;
        // }

        // text = `${text}. The default settings for this store ${
        //     currency && language ? 'are' : 'is'
        // }`;

        // if (language) {
        //     text = `${text} ${storeSettings.languages[recommendedSettings.language]}`;
        // }

        // if (currency) {
        //     if (language) {
        //         text = `${text} and `;
        //     }

        //     text = `${text} ${storeSettings.currencies[recommendedSettings.currency]}`;
        // }
        text = `${text}.<br><br>Do you want to use the default settings?`;

        $recommendationText.innerHTML = text;
        $settingsContainer.classList.add('u-hide');
        $recommendationContainer.classList.remove('u-hide');

        /**
         * TODO: refactor
         */
        if ($recommendationConfirmButtons) {
            [...$recommendationConfirmButtons].map($button => {
                $button.addEventListener(
                    'click',
                    (event: UIEvent) => {
                        const $target: HTMLElement = (event.target: any);

                        if ($target.dataset.confirm === 'true') {
                            updateUserSettings(recommendedSettings);
                        } else {
                            updateUserSettings(userSettings);
                        }

                        closeSettingsModal();
                    },
                    false
                );
            });
        }
    }
};

const checkRecommendations = settings => {
    const recommendation = find(
        COUNTRY_LANG_CURRENCY_RECOMMENDATIONS,
        r => r.country === settings.country
    );

    if (recommendation && recommendation.currency !== settings.currency) {
        return recommendation;
    }
};

const handleFormSubmit = (event: Event) => {
    event.preventDefault();

    if ($settingsForm) {
        const formData = new FormData($settingsForm);
        const newSettings: Object = {countryConfirmed: true};

        [...formData.entries()].map(([key, value]) => {
            newSettings[key] = value;
        });

        if (newSettings.country && (userSettings && newSettings.country !== userSettings.country)) {
            const recommendations = false; //checkRecommendations(newSettings);

            if (recommendations) {
                return showRecommendations(newSettings, recommendations);
            }
        }

        updateUserSettings(newSettings, true);
        closeSettingsModal();
    }
};

const handleAgeFormSubmit = (event: Event) => {
    event.preventDefault();

    if ($ageConfirmForm) {
        const formData = new FormData($ageConfirmForm);

        let ageConfirmed = false;
        let stateConfirmed = false;

        if (formData.get('user-age')) {
            ageConfirmed = formData.get('user-age') === 'Yes';
        }

        if (formData.get('user-state')) {
            stateConfirmed = true;
        }

        if (
            !ageConfirmed ||
            (userSettings && userSettings.country === 'US' && !stateConfirmed) ||
            (storeSettings.country === 'US' && !stateConfirmed)
        ) {
            if ($ageContainer && $ageFallback) {
                $ageContainer.classList.add('u-hide');
                $ageFallback.classList.remove('u-hide');
            }
        } else {
            updateUserSettings({ageConfirmed: storeSettings.ageMinimum});
            closeSettingsModal();

            // if ($ageContainer && $settingsContainer) {
            //     setTimeout(() => {
            //         $ageContainer.classList.add('u-hide');
            //         $settingsContainer.classList.remove('u-hide');
            //     }, 500);
            // }

            if (window.ageConfirmationCallback) {
                const [func, args] = window.ageConfirmationCallback;

                setTimeout(() => {
                    func(args);
                    window.ageConfirmationCallback = null;
                }, 1000);
            }
        }
    }
};

const fetchUsersLocation = () => {
    return new Promise((resolve, reject) => {
        fetch(`https://ipapi.co/json/`)
            .then(response => response.json())
            .then(resolve)
            .catch(reject);
    });
};

const saveFromUrlParams = () => {
    if (typeof window !== 'undefined') {
        const params = new URLSearchParams(window.location.search);
        const settingsParam = params.get('_us');

        if (settingsParam) {
            const rawSettings = decodeURIComponent(settingsParam);

            if (rawSettings) {
                updateUserSettings(JSON.parse(rawSettings));

                if (window.history) {
                    window.history.pushState('', '', window.location.pathname);
                }
            }
        }
    }
};

const checkUserSettings = () => {
    const savedSettings = Cookie.getJSON('userSettings');

    if ( ! userSettings) {
        updateUserSettings(storeSettings)
    }

    if (savedSettings) {
        userSettings = {...userSettings, ...savedSettings};
    }

    /**
     * Has the stores age requirement changed since the user's last visit?
     */
    if (storeSettings.ageMinimum !== userSettings.ageMinimum) {
        updateUserSettings({
            ageMinimum: storeSettings.ageMinimum
        });
    }

    if (userSettings && userSettings.currency !== storeSettings.currency) {
        convertAllPrices(userSettings.currency);
    }

    updateHeaderSettingsCTA();

    if ( ! userSettings.countryConfirmed) {
        fetchUsersLocation().then(handleLocationLookup)
    }
};

const handleLocationLookup = (location) => {
    if (location.country === 'US' || storeSettings.country === 'US') {
        updateAgeRequirementFields(21);
    }

    // Confirming the visitors country here as we are forcing a
    // store redirect, this will prevent future lookups. Once
    // we update these settings it will attempt a redirect
    updateUserSettings({
        country: location.country,
        // Force local store currency, or default to current store
        currency: STORE_LOCAL_CURRENCIES[brand][location.country] || storeSettings.currency,
        countryConfirmed: true
    }, true)
}

const addEvents = () => {
    if ($ageConfirmForm) {
        $ageConfirmForm.addEventListener('submit', handleAgeFormSubmit, false);
    }

    if ($ctaButton) {
        [...$ctaButton].map($button =>
            $button.addEventListener('click', handleSettingsModalClick, false)
        );
    }

    if ($settingsForm) {
        $settingsForm.addEventListener('submit', handleFormSubmit, false);
    }
};

const cacheElements = () => {
    $ageContainer = ((document.querySelector('.js-user-age-container'): any): HTMLElement);
    $ageConfirmForm = ((document.querySelector('.js-user-age-form'): any): HTMLFormElement);
    $ageFallback = ((document.querySelector('.js-user-age-fallback'): any): HTMLElement);
    $ageMinimum = ((document.querySelector('.js-user-age-minimum'): any): HTMLElement);
    $ctaButton = ((document.querySelectorAll('.js-user-settings-open'): any): NodeList<HTMLButtonElement>);
    $ctaButtonText = ((document.querySelectorAll('.js-user-settings'): any): NodeList<HTMLSpanElement>);
    $flagImage = ((document.querySelectorAll('.js-user-flag'): any): NodeList<HTMLImageElement>);
    $prices = ((document.querySelectorAll('.js-price'): any): NodeList<HTMLElement>);
    $settingsContainer = ((document.querySelector('.js-user-settings-container'): any): HTMLElement);
    $settingsForm = ((document.querySelector('.js-user-settings-form'): any): HTMLFormElement);
    $stateContainer = ((document.querySelector('.js-user-state-container'): any): HTMLElement);
    $stateSelector = ((document.querySelector('.js-user-state'): any): HTMLSelectElement);
    $countrySelector = ((document.querySelector('.js-user-country'): any): HTMLSelectElement);
    $currencySelector = ((document.querySelector('.js-user-currency'): any): HTMLSelectElement);
};

const populateUserSettingsDropdowns = () => {
    if ($countrySelector) {
        populateDropdown($countrySelector, storeSettings.countries)
    }

    if ($currencySelector) {
        populateDropdown($currencySelector, storeSettings.currencies)
    }
}

const populateDropdown = ($dropdown, items) => {
    for (let key in items) {
        let $option = document.createElement('option')
        $option.textContent = items[key]
        $option.value = key
        $dropdown.append($option)
    }
}

export const init = () => {
    cacheElements();
    populateUserSettingsDropdowns();
    addEvents();
    modalInit();
    saveFromUrlParams();
    checkUserSettings();
};

export default {init};
