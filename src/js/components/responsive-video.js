/*
 * @flow
 */
let $iframes;

const wrap = ($element: HTMLElement) => {
    const $wrapper = document.createElement('div');

    if ($element.parentNode) {
        $element.parentNode.insertBefore($wrapper, $element);

        $wrapper.classList.add('c-responsive-video');
        $wrapper.appendChild($element);
    }
};

const addWrappers = () => {
    if ($iframes) {
        [...$iframes].map(wrap);
    }
};

const cacheElements = () => {
    $iframes = ((document.querySelectorAll(
        '.js-iframe-container iframe'
    ): any): NodeList<HTMLElement>);
};

const init = () => {
    cacheElements();
    addWrappers();
};

export default {init};
