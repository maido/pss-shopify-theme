/*
 * @flow
 */
import React from 'react';
import {render} from 'react-dom';
import ProductPreview from './react/ProductPreview';

let $previewButtons;

const renderComponent = () => {
    const $container = document.querySelector('#product-preview-container');

    if ($container) {
        render(
            <ProductPreview
                ref={component => (window.globalComponents.ProductPreview = component)}
            />,
            $container
        );
    }
};

const handlePreviewButtonClick = (event: Event) => {
    const $target: HTMLElement = (event.target: any);
    const productHandle = $target.dataset.productHandle;

    if (window.globalComponents.ProductPreview) {
        window.globalComponents.ProductPreview.updateProduct(productHandle);
    }
};

const addEvents = () => {
    if ($previewButtons) {
        [...$previewButtons].map($button =>
            $button.addEventListener('click', handlePreviewButtonClick)
        );
    }
};

const cacheElements = () => {
    $previewButtons = ((document.querySelectorAll('.js-product-preview'): any): NodeList<
        HTMLButtonElement | HTMLLinkElement
    >);
};

const init = () => {
    cacheElements();
    addEvents();
    renderComponent();
};

export default {init};
