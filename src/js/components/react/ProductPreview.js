/*
 * @flow
 */
import React, {Component} from 'react';
import {addToCart, showCartSummaryModal} from '../cart';
import {getFetchParams, getPrice} from '../../services/helpers';

type Props = {productHandle?: string};
type State = {product?: ShopifyProduct | null, selectedVariantId: number | null};

class ProductPreview extends Component<Props, State> {
    state = {product: null, selectedVariantId: null};

    updateProduct = (productHandle: string) => {
        fetch(`/products/${productHandle}.js`, getFetchParams())
            .then(response => response.json())
            .then(product => this.setState({product}));
    };

    render() {
        const {product, selectedVariantId} = this.state;

        if (!product) {
            return null;
        }

        return (
            <div>
                <h2 className="u-heading u-mb">{product.title}</h2>
            </div>
        );
    }
}

export default ProductPreview;
