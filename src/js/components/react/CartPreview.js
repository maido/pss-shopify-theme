/*
 * @flow
 */
import {h, Fragment} from 'preact';
import {useEffect, useState} from 'preact/hooks';
import get from 'lodash/get';
import {validateAgeCheckedProducts} from '../cart';
import {getFetchParams, formatMoney} from '../../services/helpers';
import currencyFormats from './../currency-formats'

type Props = {data: ShopifyCart};

const translations = get(window, 'globalData.translations.cart', {});
const storeSettings = get(window, 'globalData.storeSettings', {});
const moneyFormat = (currencyFormats[storeSettings.currency]) ? currencyFormats[storeSettings.currency].money_format : undefined


const filteredRecommendedItems = (recommendedItems: Array<Object>, cart: ShopifyCart) => {
    const itemsInCart = cart.items.map(item => item.product_title);

    return recommendedItems.filter((item, index) => index < 3 && !itemsInCart.includes(item.title));
};

const CartPreview = ({initialData}: Props) => {
    const [data, setData] = useState(initialData);
    const [highlightedVariantId, setHighlightedVariantId] = useState(0);
    const [recommendedItems, setRecommendedItems] = useState([]);
    const [buttonStatus, setButtonStatus] = useState('Checkout');

    const updateCart = (
        data: ShopifyCart,
        highlightedVariantId: number,
        recommendedItems: Array<Object> = []
    ) => {
        setData(data);
        setHighlightedVariantId(highlightedVariantId);
        // setRecommendedItems(filteredRecommendedItems(recommendedItems, data));
    };

    useEffect(() => {
        window.globalComponents.updateCartPreview = updateCart;
    });

    const handleCheckoutClick = () => {
        setButtonStatus('Loading');
        validateAgeCheckedProducts(data.items).then(response => (window.location = `/checkout`));
    };

    const viewRecommendedProduct = (handle: string) => (window.location = `/products/${handle}`);

    return (
        <div>
            <h2 className="u-heading u-h3 u-color-primary u-mb">{translations.title}</h2>

            {data.items.length > 0 && (
                <Fragment>
                    <div className="c-modal__scroller">
                        {data.items.length > 0 &&
                            data.items.map(item => (
                                <div
                                    className={`u-flex u-align-items-center ${
                                        item.variant_id === highlightedVariantId
                                            ? 'u-highlight-change'
                                            : ''
                                    }`}
                                    key={item.title}
                                >
                                    <a href={item.url}>
                                        <img
                                            src={item.image}
                                            alt={`${item.title} product image`}
                                            className="u-pr"
                                            style={{
                                                objectFit: 'cover',
                                                height: 100,
                                                width: 100
                                            }}
                                        />
                                    </a>
                                    <div className="u-flex-grow-1">
                                        <a
                                            href={item.url}
                                            className="u-h5 u-heading u-display-block"
                                        >
                                            {item.product_title}
                                        </a>
                                        {item.variant_title && (
                                            <span className="u-heading u-heading--caps u-color-primary">
                                                {item.variant_title}&nbsp;&nbsp;
                                            </span>
                                        )}
                                        {item.quantity > 1 && (
                                            <small>
                                                <span
                                                    className="u-grey-dark"
                                                      dangerouslySetInnerHTML={{__html: formatMoney(item.price, moneyFormat)}}
                                                >
                                                </span>
                                                <span className="u-color-grey">
                                                    (x{item.quantity})
                                                </span>
                                            </small>
                                        )}
                                        {item.quantity === 1 && (
                                            <span
                                                className="u-color-grey-dark"
                                                  dangerouslySetInnerHTML={{__html: formatMoney(item.price, moneyFormat)}}
                                            >
                                            </span>
                                        )}
                                    </div>
                                </div>
                            ))}
                    </div>
                    {recommendedItems.length > 0 && (
                        <div className="u-mt u--mb">
                            <hr className="u-hr u-mb" />
                            <span className="u-label u-color-primary">
                                {translations.recommendationTitle}
                            </span>

                            <div className="u-flex u-mt-" style={{marginLeft: -12}}>
                                {recommendedItems.map(item => (
                                    <div
                                        className="u-ml-"
                                        onClick={() => viewRecommendedProduct(item.handle)}
                                    >
                                        <div
                                            style={{
                                                backgroundColor: '#fff',
                                                backgroundImage: `url(${item.image})`,
                                                backgroundRepeat: 'no-repeat',
                                                backgroundPosition: 'center center',
                                                backgroundSize: 'contain',
                                                border: '1px solid #f3f6f6',
                                                cursor: 'pointer',
                                                width: 110,
                                                height: 130
                                            }}
                                            className="u-mb-"
                                        />
                                        <small
                                            className="u-display-block u-heading"
                                            style={{lineHeight: 1.4}}
                                        >
                                            {item.title}
                                        </small>
                                    </div>
                                ))}
                            </div>
                            <hr className="u-hr u-mt" />
                        </div>
                    )}

                    <footer className="u-mt u-mt+@mobile u-text-align-right c-animated-item u-in-viewport">
                        <h3 className="u-h4 u-heading" data-order="2">
                            <span className="u-heading u-heading--caps">
                                {translations.subtotal}
                            </span>{' '}
                            <span dangerouslySetInnerHTML={{__html: formatMoney(data.total_price, moneyFormat)}}></span>
                        </h3>
                        <small data-order="3" class="u-color-grey">
                            {translations.shipping}
                        </small>

                        <a
                            href="/cart"
                            className="c-button c-button--primary c-button--ghost u-1/1 u-mt-"
                            data-order="4"
                        >
                            <span className="c-button__text">{translations.viewCart}</span>
                        </a>
                    </footer>
                </Fragment>
            )}
            {data.items.length === 0 && (
                <Fragment>
                    <p>{translations.empty}</p>
                    <footer className="u-mt">
                        <a
                            href="/products"
                            className="c-button c-button--primary c-button--ghost u-1/1"
                        >
                            <span className="c-button__text">{translations.continue}</span>
                        </a>
                    </footer>
                </Fragment>
            )}
        </div>
    );
};

export default CartPreview;
