/*
 * @flow
 */
import clamp from 'lodash/clamp';
let $marquee;

const handleWindowScroll = () => {
    const y = window.pageYOffset / 2;
    const maxWidth = $marquee.getBoundingClientRect().width;
    const offset = maxWidth > 800 ? clamp(y, 0, maxWidth) : y;

    $marquee.style.transform = `translateX(${offset * -1}px)`;
};

const cacheElements = () => {
    $marquee = ((document.querySelector('.js-marquee'): any): HTMLElement);
};

const addEvents = () => {
    window.addEventListener('scroll', handleWindowScroll, false);
    window.addEventListener('touchmove', handleWindowScroll, false);
};

const init = () => {
    cacheElements();

    if ($marquee) {
        addEvents();
    }
};

export default {init};
