/**
 * @flow
 */
import find from 'lodash/find';
import get from 'lodash/get';
import orderBy from 'lodash/orderBy';
import 'url-search-params-polyfill';
import {updateHistory} from '../services/helpers';

const allProducts = get(window, 'globalData.allProducts', []);

let $productsContainer;
let $sortSelect;

const renderSortedProducts = products => {
    if ($productsContainer) {
        const $products = $productsContainer.children;
        const $elements = document.createDocumentFragment();

        products.map(product => {
            const $product = find($products, $p => parseInt($p.id) === product.id);

            if ($product) {
                $elements.appendChild($product.cloneNode(true));
            }
        });

        $productsContainer.innerHTML = null;
        $productsContainer.appendChild($elements);
    }
};

const sortProducts = (options = '') => {
    const [type, direction] = options.split('-');
    const sortedProducts = orderBy(allProducts, [type], [direction]);

    renderSortedProducts(sortedProducts);
};

const handleSortChange = (event: Event) => {
    const $target = ((event.target: any): HTMLSelectElement);

    if ($target) {
        sortProducts($target.value);
        window.history.pushState('', '', `?sort-by=${encodeURIComponent($target.value)}`);
    }
};

const getDefaultSort = () => {
    const searchParams = new URLSearchParams(window.location.search);

    if (searchParams.has('sort-by')) {
        sortProducts(decodeURIComponent(searchParams.get('sort-by')));
    }
};

const addEvents = () => {
    if ($sortSelect) {
        $sortSelect.addEventListener('change', handleSortChange, false);
    }
};

const cacheElements = () => {
    $productsContainer = ((document.querySelector('.js-products-container'): any): HTMLDivElement);
    $sortSelect = ((document.querySelector('.js-sort-by'): any): HTMLSelectElement);
};

const init = () => {
    cacheElements();
    addEvents();
    getDefaultSort();
};

export default {init};
