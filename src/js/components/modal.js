/*
 * @flow
 */
import anime from 'animejs';
import eq from 'lodash/eq';
import get from 'lodash/get';
import throttle from 'lodash/throttle';
import {intendedTargetElement} from '../services/helpers';
import tracking, {track} from './tracking';
import modalAnimations from './modal-animations';

let $body;
let $html;
let $modalCloseButton;
let $modalOpenButton;
let $modalContainer;
let $modalWrapper;

const ANIMATION_DURATION = 150;
const ANIMATION_OFFSET = 40;
let handleOutsideClickEvents = () => {};

const setDisplayStyle = () => {
    if ($modalContainer && $modalWrapper) {
        $modalContainer.style.visibility = 'hidden';

        const modalIsTallerThanWindow =
            $modalWrapper.getBoundingClientRect().height > window.innerHeight;

        $modalContainer.classList.toggle('c-modal--no-flex', modalIsTallerThanWindow);
        $modalContainer.style.visibility = 'visible';
    }
};

const runOpenAnimations = () => {
    if ($modalContainer) {
        const animations = modalAnimations[$modalContainer.dataset.modal];

        if (animations && animations.open) {
            animations.open();
        }
    }
};

const addOutsideClickEvents = () => {
    document.addEventListener('click', handleOutsideClickEvents, false);
};

const removeOutsideClickEvents = () => {
    document.removeEventListener('click', handleOutsideClickEvents, false);
};

export const closeModal = (): void => {
    if ($body && $modalContainer && $modalWrapper && $html) {
        removeOutsideClickEvents();

        $body.classList.remove('u-no-scroll');
        $html.classList.remove('u-no-scroll');

        anime({
            opacity: [1, 0],
            targets: $modalCloseButton,
            translateY: [0, ANIMATION_OFFSET / 2]
        });
        anime({
            easing: 'spring(1, 100, 25, 0)',
            opacity: [1, 0],
            targets: $modalWrapper,
            translateY: [0, ANIMATION_OFFSET]
        });
        anime({
            complete() {
                $modalContainer.classList.add('u-hide');
            },
            easing: 'spring(2, 200, 25, 0)',
            opacity: [1, 0],
            targets: $modalContainer
        });

        setTimeout(() => {
            $modalContainer.classList.add('u-hide');
        }, 1000);
    }
};

handleOutsideClickEvents = (event: Event) => {
    const $target: HTMLElement = (event.target: any);
    const expectedClass = 'c-modal';

    if (
        $target &&
        $target.classList.contains(expectedClass) &&
        $target.dataset.modal !== 'user-settings'
    ) {
        closeModal();
    }
};

export const openModal = (isNonInteractive: boolean = false): void => {
    if ($modalContainer && $modalWrapper && $body && $html) {
        runOpenAnimations();
        setDisplayStyle();
        addOutsideClickEvents();

        anime({
            begin() {
                $body.classList.add('u-no-scroll');
                $html.classList.add('u-no-scroll');
                $modalContainer.classList.remove('u-hide');
            },
            easing: 'spring(1, 200, 25, 0)',
            opacity: [0, 1],
            targets: $modalContainer
        });
        anime({
            easing: 'spring(1, 200, 25, 0)',
            delay: ANIMATION_DURATION,
            opacity: [0, 1],
            targets: $modalWrapper,
            translateY: [ANIMATION_OFFSET, 0]
        });
        anime({
            delay: ANIMATION_DURATION / 2,
            opacity: [0, 1],
            targets: $modalCloseButton,
            translateY: [ANIMATION_OFFSET / 2, 0]
        });

        track('event', {
            ec: 'Overlay',
            ea: 'click',
            el: `Opened ${get($modalContainer, 'dataset.modal')}`,
            ni: isNonInteractive ? 1 : null
        });
    }
};

export const setActiveModal = (id: string) => {
    const $targetModal = document.querySelector(`.js-modal[data-modal="${id}"]`);

    if ($targetModal) {
        $modalContainer = $targetModal;
        $modalWrapper = (($targetModal.querySelector('.js-modal-wrapper'): any): HTMLElement);
    }
};

const handleOpenModal = (event: Event) => {
    event.preventDefault();

    const $target: HTMLElement = (intendedTargetElement('js-modal-open', event.target): any);

    if ($target) {
        const targetModal = get($target, 'dataset.modal');

        if (targetModal) {
            setActiveModal(targetModal);
            openModal();
        }
    }
};

const handleCloseModal = (event: Event) => {
    event.preventDefault();

    const $target: HTMLElement = (intendedTargetElement('js-modal-close', event.target): any);

    if ($target) {
        const targetModal = get($target, 'dataset.modal');
        const closeMatchesTarget = eq(get($modalContainer, 'dataset.modal'), targetModal);

        if (closeMatchesTarget) {
            setActiveModal('');
            closeModal();
        }
    }
};

const detectEscapeKeyUp = event => {
    if (event.keyCode === 27) {
        closeModal();
    }
};

const addEvents = () => {
    if ($modalOpenButton) {
        [...$modalOpenButton].map($button =>
            $button.addEventListener('click', handleOpenModal, false)
        );
    }

    if ($modalCloseButton) {
        [...$modalCloseButton].map($button =>
            $button.addEventListener('click', handleCloseModal, false)
        );
    }

    window.addEventListener('keyup', detectEscapeKeyUp, false);
    window.addEventListener('resize', throttle(setDisplayStyle, 250), false);
};

const cacheElements = () => {
    $body = ((document.body: any): HTMLElement);
    $html = ((document.querySelector('html'): any): HTMLElement);
    $modalCloseButton = ((document.querySelectorAll(
        '.js-modal-close'
    ): any): NodeList<HTMLButtonElement>);
    $modalOpenButton = ((document.querySelectorAll(
        '.js-modal-open'
    ): any): NodeList<HTMLButtonElement>);
};

export const init = () => {
    tracking.init(false);
    cacheElements();
    addEvents();
};

export default {init};
