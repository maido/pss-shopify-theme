/*
 * @flow
 */
import {h, render} from 'preact';
import React from 'preact/compat';
import get from 'lodash/get';
import CartPreview from './react/CartPreview';

const renderComponent = () => {
    const $container = document.querySelector('#cart-preview-container');

    if ($container) {
        const cartData = get(window, 'globalData.cart');

        render(<CartPreview initialData={cartData} />, $container);
    }
};

const init = () => {
    renderComponent();
};

export default {init};
