/*
 * @flow
 */
import lazysizes from 'lazysizes';

const handleItemLoaded = (event: Event) => {
    const $target = ((event.target: any): HTMLElement);
    console.log({$target});
};

const handleItemUnveiled = (event: Event) => {
    const $target = ((event.target: any): HTMLElement);
    console.log({$target});
    if ($target && $target.dataset) {
        const image = $target.getAttribute('data-src');
        console.log(image);
        if (image) {
            $target.style.backgroundImage = `url(${image})`;
        } else {
            console.log('no image');
        }
    } else {
        console.log('no target or dataset');
    }
};

const init = () => {
    document.addEventListener('lazyloaded', handleItemLoaded, false);
    // document.addEventListener('lazybeforeunveil', handleItemUnveiled, false);
};

export default {init};
