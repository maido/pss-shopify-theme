/**
 * @flow
 */
import anime from 'animejs';
import find from 'lodash/find';
import get from 'lodash/get';
import Cookie from 'js-cookie';
import {
    addToCart,
    getCart,
    init as cartInit,
    showCartSummaryModal,
    trackAddToCart,
    updateCartSummary,
    validateAgeCheckedProducts
} from './cart';
import {getPrice, intendedTargetElement, updateHistory} from '../services/helpers';

type AddToCartButtonProperties = {
    label?: string,
    'product-handle'?: string,
    quantity?: string,
    'variant-id'?: string
};

const cart = get(window, 'globalData.cart');
const product = get(window, 'globalData.product');
const relatedProducts = get(window, 'globalData.relatedProducts');
const translations = get(window, 'globalData.translations.product');
let $addButtons;
let $galleryContainer;
let $galleryPhotos;
let $galleryThumbs;
let $quantityInputs;
let $quantityToggles;
let $variantSelectButtons;
let $variantSelects;
let $cartCheckoutButtons;
let $shippingConsentCheckbox;

const trackProductView = (id: number, price: number) => {
    if (window.fbq && id && price) {
        window.fbq('track', 'ViewContent', {
            content_ids: [parseInt(id)],
            content_type: 'product',
            currency: 'GBP',
            value: parseInt(price) / 100
        });
    }
};

// const isSoldOut = (product: ShopifyProduct) => {
//     const productAvailable = product.available;
//     const variantAvailable = get(product, 'variants[0].available');

//     return !productAvailable | !variantAvailable;
// };

const updateAddButtons = (properties: AddToCartButtonProperties, productHandle = '') => {
    if ($addButtons) {
        [...$addButtons]
            .filter($button => {
                if (productHandle !== '') {
                    return $button.dataset.productHandle === productHandle;
                } else {
                    return true;
                }
            })
            .map($button => {
                const $label = $button.querySelector('.js-add-button-label');
                const $price = $button.querySelector('.js-add-button-price');

                Object.keys(properties).map(property =>
                    $button.setAttribute(`data-${property}`, properties[property])
                );

                const variant = find(product.variants, {id: parseInt(properties['variant-id'])});

                if (variant) {
                    if (variant.available) {
                        $button.removeAttribute('disabled');
                        $label.innerText = ttranslations.add;
                    } else {
                        $button.setAttribute('disabled', 'disabled');
                        $label.innerText = translations.soldOut;
                    }
                }

                const quantity = properties.quantity || 1;
                $price.innerText = getPrice($button.dataset.price * quantity);
            });
    }
};

const updateVariantButtons = (selectedVariantId: string) => {
    if ($variantSelectButtons) {
        [...$variantSelectButtons].map($button => {
            $button.classList.toggle(
                'c-button--selected',
                $button.dataset.variantId === selectedVariantId
            );
        });
    }
};

const getProductStatus = (productHandle: string, variantId: number) => {
    fetch(`/products/${productHandle}.js`, {
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
    })
        .then(response => response.json())
        .then(response => updateAddButtons({'variant-id': `${variantId}`}, productHandle));
};

const addProductToCart = (productHandle: string, variantId: number, quantity: number) => {
    return new Promise((resolve, reject) => {
        addToCart(variantId, quantity)
            .then(getCart)
            .then(cartData => {
                updateCartSummary(cartData);
                showCartSummaryModal(cartData, variantId, relatedProducts);
                // getProductStatus(productHandle, variantId);
                resolve();
            })
            .catch(() => (window.location.href = '/cart'));
    });
};

const handleAddToCartClick = (event: Event) => {
    const $target: HTMLElement = (intendedTargetElement('js-add-to-cart', event.target): any);

    if ($target) {
        event.preventDefault();
        $target.blur();

        const {price, productHandle, quantity, source, variantId} = get($target, 'dataset');
        const $label = $target.querySelector('.js-add-button-label');

        if (productHandle && variantId) {
            let previousLabel = $label.innerText;

            $label.innerText = translations.adding;

            addProductToCart(productHandle, parseInt(variantId), parseInt(quantity))
                .then(() => {
                    trackAddToCart(productHandle, variantId, price, source);
                    $label.innerText = translations.added;
                    setTimeout(() => {
                        $label.innerText = translations.addAgain;
                    }, 1000);
                })
                .catch(() => ($label.innerText = previousLabel));
        }
    }
};

const handleRestrictedAddToCartClick = (event: Event) => {
    event.preventDefault();

    const userSettings = Cookie.getJSON('userSettings');

    if (
        !userSettings ||
        (userSettings && !userSettings.ageConfirmed) ||
        (userSettings && userSettings.ageConfirmed !== userSettings.ageMinimum)
    ) {
        if (window.requestUserSettingsConfirmation) {
            window.ageConfirmationCallback = [handleAddToCartClick, event];
            window.requestUserSettingsConfirmation('age');
        }
    } else if (userSettings && userSettings.ageConfirmed === userSettings.ageMinimum) {
        handleAddToCartClick(event);
    }
};

const updateURLWithNewProduct = (handle: string): void => updateHistory(`/products/${handle}`);

const handleVariantButtonClick = (event: Event) => {
    const $target = ((event.target: any): HTMLButtonElement);

    if ($target) {
        updateVariantButtons($target.dataset.variantId);
        updateAddButtons({'variant-id': $target.dataset.variantId}, product.handle);
        updateURLWithNewProduct(`${product.handle}?variant=${$target.dataset.variantId}`);
    }
};

const handleVariantSelectChange = (event: Event) => {
    const $target = ((event.target: any): HTMLSelectElement);

    if ($target) {
        updateAddButtons({'variant-id': $target.value}, product.handle);
        updateURLWithNewProduct(`${product.handle}?variant=${$target.value}`);
    }
};

const handleQuantityChange = (event: Event) => {
    const $target = ((event.target: any): HTMLInputElement);

    if ($target) {
        updateAddButtons({quantity: $target.value});
    }
};

const handleQuantityToggle = (event: Event) => {
    const $target = ((event.target: any): HTMLInputElement);

    if ($target && $quantityInputs) {
        let newQuantity;

        [...$quantityInputs].map($input => {
            newQuantity =
                $target.dataset.action === 'increase'
                    ? parseInt($input.value) + 1
                    : parseInt($input.value) - 1;

            if (newQuantity < 1) {
                newQuantity = 1;
            }

            $input.value = newQuantity;
        });

        updateAddButtons({quantity: newQuantity});
    }
};

const handleGalleryThumbClick = (event: Event) => {
    const $target = ((event.target: any): HTMLInputElement);

    if ($target) {
        const index = parseInt($target.dataset.index);
        const offset = (index - 1) * 100 * -1;

        if ($galleryContainer) {
            anime({
                easing: 'spring(1, 100, 25, 0)',
                targets: $galleryContainer,
                translateX: `${offset}%`
            });
        }

        if ($galleryThumbs) {
            [...$galleryThumbs].map($thumb =>
                $thumb.classList.toggle('is-active', parseInt($thumb.dataset.index) === index)
            );
        }

        if (index > 1) {
            [...$galleryPhotos][index - 1].style.backgroundImage = `url(${
                [...$galleryPhotos][index - 1].dataset.imageSource
            })`;
        }
    }
};

const handleCheckout = () => {
    if (cart.items) {
        validateAgeCheckedProducts(cart.items).then(response => (window.location = `/checkout`));
    }
};

const handleShippingConsentToggle = (event: Event) => {
    const $target = ((event.target: any): HTMLInputElement);

    if ($cartCheckoutButtons) {
        if ($target.checked === true) {
            [...$cartCheckoutButtons].map($button => $button.removeAttribute('disabled'));
        } else {
            [...$cartCheckoutButtons].map($button => $button.setAttribute('disabled', 'disabled'));
        }
    }
};

const addEvents = () => {
    if ($addButtons) {
        [...$addButtons].map($button => {
            if ($button.dataset.ageCheck && $button.dataset.ageCheck === 'true') {
                $button.addEventListener('click', handleRestrictedAddToCartClick);
            } else {
                $button.addEventListener('click', handleAddToCartClick);
            }
        });
    }

    if ($galleryThumbs) {
        [...$galleryThumbs].map($thumb =>
            $thumb.addEventListener('click', handleGalleryThumbClick)
        );
    }

    if ($quantityInputs) {
        [...$quantityInputs].map($input => $input.addEventListener('change', handleQuantityChange));
    }

    if ($quantityToggles) {
        [...$quantityToggles].map($button =>
            $button.addEventListener('click', handleQuantityToggle)
        );
    }

    if ($variantSelectButtons) {
        [...$variantSelectButtons].map($button =>
            $button.addEventListener('click', handleVariantButtonClick)
        );
    }

    if ($variantSelects) {
        [...$variantSelects].map($select =>
            $select.addEventListener('change', handleVariantSelectChange)
        );
    }

    if ($cartCheckoutButtons) {
        [...$cartCheckoutButtons].map($button => $button.addEventListener('click', handleCheckout));
    }

    if ($shippingConsentCheckbox) {
        $shippingConsentCheckbox.addEventListener('click', handleShippingConsentToggle, false);
    }
};

const cacheElements = () => {
    $addButtons = ((document.querySelectorAll(
        '.js-add-to-cart'
    ): any): NodeList<HTMLButtonElement>);
    $cartCheckoutButtons = ((document.querySelectorAll(
        '.js-cart-checkout'
    ): any): NodeList<HTMLButtonElement>);
    $galleryContainer = ((document.querySelector('.js-gallery'): any): HTMLElement);
    $galleryPhotos = ((document.querySelectorAll('.js-gallery-photo'): any): NodeList<HTMLElement>);
    $galleryThumbs = ((document.querySelectorAll(
        '.js-gallery-thumb'
    ): any): NodeList<HTMLButtonElement>);
    $quantityInputs = ((document.querySelectorAll(
        '.js-variant-quantity'
    ): any): NodeList<HTMLInputElement>);
    $quantityToggles = ((document.querySelectorAll(
        '.js-quantity-toggle'
    ): any): NodeList<HTMLButtonElement>);
    $shippingConsentCheckbox = ((document.querySelector(
        '.js-shipping-consent'
    ): any): HTMLInputElement);
    $variantSelectButtons = ((document.querySelectorAll(
        '.js-variant-button'
    ): any): NodeList<HTMLButtonElement>);
    $variantSelects = ((document.querySelectorAll(
        '.js-variant-select'
    ): any): NodeList<HTMLSelectElement>);
};

const init = () => {
    cacheElements();
    addEvents();
    cartInit();
};

export default {init};
