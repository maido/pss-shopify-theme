/*
 * @flow
 */
import GAnalytics from 'ganalytics';
import get from 'lodash/get';
import {intendedTargetElement} from '../services/helpers';

let $events;
let ga;

export const track = (type: string, parameters: Object) => {
    ga.send(type, parameters);
};

const handleElementClick = (event: Event) => {
    const $target: HTMLElement = (intendedTargetElement('js-track-event', event.target): any);

    if ($target) {
        const ec = get($target, 'dataset.category');
        const el = get($target, 'dataset.label');

        if (ec && el) {
            track('event', {ec, el});
        }
    }
};

const cacheElements = () => {
    $events = ((document.querySelectorAll('.js-track-event'): any): NodeList<HTMLElement>);
};

const addEvents = () => {
    if ($events) {
        [...$events].map($event => $event.addEventListener('click', handleElementClick));
    }
};

const setupGA = () => {
    if (typeof window !== 'undefined') {
        if (window._ga) {
            ga = window._ga;
        } else {
            ga = new GAnalytics(get(window, 'globalData.gaID', ''));
            window._ga = ga;
        }
    }
};

const init = (listen: boolean = true) => {
    setupGA();

    if (ga && listen) {
        cacheElements();
        addEvents();
    }
};

export default {init};
