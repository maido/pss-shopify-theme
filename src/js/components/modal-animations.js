/*
 * @flow
 */
import anime from 'animejs';

const defaultModal = {
    open() {
        anime
            .timeline()
            .add({
                duration: 500,
                elasticity: 100,
                offset: '+=450',
                opacity: [0, 1],
                targets: '[data-modal] .js-title',
                translateY: [24, 0]
            })
            .add({
                duration: 500,
                elasticity: 100,
                opacity: [0, 1],
                offset: '-=350',
                targets: '[data-modal] .js-cta',
                translateY: [6, 0]
            });
    }
};

export default {
    defaultModal
};
