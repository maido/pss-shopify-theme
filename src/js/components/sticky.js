/*
 * @flow
 */
import anime from 'animejs';

let $stickyItem;

let lastKnownScrollY = 0;
let currentScrollY = 0;
let isStuck = true;
let isScrolling = false;

const setBackgroundStyle = isStuck => {
    if ($stickyItem) {
        if (isStuck) {
            $stickyItem.style.backgroundColor = '#fff';
            $stickyItem.style.boxShadow = '0 2px 10px rgba(0,0,0, 0.07)';
        } else {
            $stickyItem.style.backgroundColor = 'transparent';
            $stickyItem.style.boxShadow = 'none';
        }
    }
};

const addSticky = () => {
    if ($stickyItem) {
        anime({
            easing: 'spring(1, 100, 30, 0)',
            targets: $stickyItem,
            translateY: 0
        });
    }
};

const removeSticky = () => {
    if ($stickyItem) {
        anime({
            easing: 'spring(1, 100, 30, 0)',
            targets: $stickyItem,
            translateY: -140
        });
    }
};

const handleStickyChange = () => {
    const scrollDifference = currentScrollY / lastKnownScrollY;

    if (scrollDifference < 1 && !isStuck) {
        isStuck = true;

        addSticky();
    } else if (scrollDifference >= 1 && currentScrollY > 105 && isStuck) {
        isStuck = false;

        removeSticky();
    }

    if (currentScrollY > 0) {
        setBackgroundStyle(true);
    } else {
        setBackgroundStyle(false);
    }

    lastKnownScrollY = currentScrollY;
    isScrolling = false;
};

const requestScroll = () => {
    if (!isScrolling) {
        requestAnimationFrame(handleStickyChange);
    }

    isScrolling = true;
};

const handleWindowScroll = () => {
    currentScrollY = window.pageYOffset;

    requestScroll();
};

export const destroySticky = () => {
    removeSticky();

    window.removeEventListener('scroll', handleWindowScroll);
    window.removeEventListener('touchmove', handleWindowScroll);
};

const cacheElements = () => {
    $stickyItem = ((document.querySelector('.js-sticky'): any): HTMLElement);
};

const addEvents = () => {
    window.addEventListener('scroll', handleWindowScroll, false);
    window.addEventListener('touchmove', handleWindowScroll, false);
};

const init = () => {
    cacheElements();

    if ($stickyItem) {
        addEvents();

        lastKnownScrollY = window.pageYOffset;

        handleWindowScroll();
        handleStickyChange();
    }
};

export default {init};
