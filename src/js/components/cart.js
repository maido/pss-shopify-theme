/*
 * @flow
 */
import Cookie from 'js-cookie';
import get from 'lodash/get';
import {closeModal, init as modalInit, openModal, setActiveModal} from './modal';
import {getFetchParams} from '../services/helpers';

let $cartCount;
let $cartCheckoutButtons;

const cart = get(window, 'globalData.cart');
let cartLocked = false;
let cartSummaryModalItemTemplate;

const lockCart = () => (cartLocked = true);

const unlockCart = () => (cartLocked = false);

const shopifyRequest = (url: string, data?: Object): Promise<*> => {
    return new Promise((resolve, reject) => {
        if (!cartLocked) {
            lockCart();

            fetch(url, getFetchParams(data))
                .then(response => response.json())
                .then(response => {
                    unlockCart();
                    resolve(response);
                })
                .catch(error => {
                    unlockCart();
                    reject(error);
                });
        }
    });
};

const handleCartRefresh = () => window.location.reload();

export const trackAddToCart = (handle: string, id: number, value: string, source: string) => {
    if (window.ga) {
        window.ga('send', 'event', {
            eventCategory: 'Add to cart',
            eventAction: source,
            eventLabel: handle
        });
    }

    if (window.fbq) {
        window.fbq('track', 'AddToCart', {
            content_ids: [parseInt(id)],
            content_type: 'product',
            currency: 'GBP',
            value: parseInt(value) / 100
        });
    }
};

export const getCart = () => shopifyRequest('/cart.js');

export const updateCartSummary = (data: ShopifyCart) => {
    if ($cartCount) {
        [...$cartCount].map($count => ($count.innerText = `${data.item_count}`));
    }
};

export const showCartSummaryModal = (
    data: ShopifyCart,
    highlightedVariantId: number,
    relatedProducts: Array<Object> = []
) => {
    const updateCartPreview = get(window, 'globalComponents.updateCartPreview');

    if (updateCartPreview) {
        updateCartPreview(data, highlightedVariantId, relatedProducts);
        setActiveModal('cart-preview');
        openModal(true);
    }
};

export const validateAgeCheckedProducts = (items = []) => {
    return new Promise(resolve => {
        const userSettings = Cookie.getJSON('userSettings');

        if (userSettings && userSettings.ageConfirmed === userSettings.ageMinimum) {
            resolve();
        } else {
            Promise.all(
                items.map(item => {
                    return fetch(`/products/${item.handle}.js`, getFetchParams()).then(r =>
                        r.json()
                    );
                })
            ).then(responses => {
                const ageCheckedProducts = responses.filter(response =>
                    response.tags.includes('age_check')
                );

                if (ageCheckedProducts.length > 0) {
                    const updates = {};

                    ageCheckedProducts.map(p => (updates[p.variants[0].id] = 0));

                    fetch(
                        `/cart/update.js`,
                        getFetchParams({
                            updates
                        })
                    )
                        .then(r => r.json())
                        .then(() => resolve(ageCheckedProducts));
                } else {
                    resolve();
                }
            });
        }
    });
};

export const addToCart = (id: number, quantity: number = 1) =>
    shopifyRequest('/cart/add.js', {id, quantity});

const cacheElements = () => {
    $cartCount = ((document.querySelectorAll('.js-cart-count'): any): NodeList<HTMLElement>);
    $cartCheckoutButtons = ((document.querySelectorAll(
        '.js-cart-checkout'
    ): any): NodeList<HTMLButtonElement>);
};

export const init = () => {
    cacheElements();
    modalInit();
};

export default {init};
