/*
 * @flow
 */
import backgroundVideo from './components/background-video';
import collection from './components/collection';
import product from './components/product';
import {isMobile} from './services/helpers';
import '../sass/product.scss';

const init = () => {
    collection.init();
    product.init();
    backgroundVideo.init();
};

export default {init}
