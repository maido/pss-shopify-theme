declare module 'siema' {
    declare type SiemaConfiguration = {
        selector?: string,
        duration?: number,
        easing?: string,
        perPage?: number | Object,
        startIndex?: number,
        draggable?: boolean,
        multipleDrag?: boolean,
        threshold?: number,
        loop?: boolean,
        rtl?: boolean,
        onInit?: Function,
        onChange?: Function,
    };

    declare class Siema {
        constructor(options?: SiemaConfiguration): void;

        goTo(index: number, callback?: Function): void;
        prev(howManySlides: number, callback?: Function): void;
        next(howManySlides: number, callback?: Function): void;
        remove(index: number, callback?: Function): void;
        insert(item: HTMLElement, index: number, callback?: Function): void;
        prepend(item: HTMLElement, callback?: Function): void;
        append(item: HTMLElement, callback?: Function): void;
        destroy(restoreMarkup: boolean, callback?: Function): void;

        currentSlide: number;
    }

    declare module.exports: typeof Siema;
};
