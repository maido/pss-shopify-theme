type ShopifyCartItem = {
    discounts: Array<string>,
    discounted_price: number,
    gift_card: boolean,
    grams: number,
    handle: string,
    image: string,
    key: string,
    line_price: number,
    original_price: number,
    original_line_price: number,
    price: number,
    product_description: string,
    product_id: number,
    product_title: string,
    product_type: string,
    properties: string,
    quantity: number,
    requires_shipping: boolean,
    sku: string,
    title: string,
    total_discount: number,
    url: string,
    variant_id: number,
    variant_options: Array<string>,
    variant_title: string,
    vendor: string
};

type ShopifyCart = {
    attributes: Object,
    items: Array<ShopifyCartItem>,
    item_count: number,
    note: null,
    original_total_price: number,
    requires_shipping: number,
    token: string,
    total_discount: number,
    total_price: number,
    total_weight: number
};

type ShopifyProductVariant = {
    available: boolean,
    barcode: string,
    compare_at_price: number | null,
    featured_image: string | null,
    id: number,
    inventory_management: string | null,
    name: string,
    option1: string | null,
    option2: string | null,
    option3: string | null,
    options: Array<string>,
    price: number,
    public_title: string | null,
    requires_shipping: boolean,
    sku: string,
    taxable: boolean,
    title: string,
    weight: number
};

type ShopifyProduct = {
    available: boolean,
    description: string,
    handle: string,
    id: number,
    price: number,
    tags: Array<string>,
    title: string,
    type: string,
    variants: Array<ShopifyProductVariant>
};
