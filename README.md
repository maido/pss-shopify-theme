# Pollen/Smith & Sinclair - Shopify

This Shopify theme is built to support multiple Shopify stores for both Pollen and Smith & Sinclair. The stores are split by country, with each having stores in UK, Europe and the US. Both stores have age restrictions on them, but the type of restrictions differ slightly for each store.

## ⬇️ Installing / Getting started

To get started, `cd` into your working/dev folder.

```shell
git clone git@bitbucket.org:maido/pss-shopify-theme
cd pss-shopify-theme
```

### Shopify Theme

To start developing the theme, you must add the necessary API passwords in `./config.yml` for both staging and production stores. This allows you to authorise and sync content and source files between your local and live (staging or production) environments.

To find the passwords, follow these steps:

-   Navigate to the store's admin
-   Click 'Apps' from the main navigation on the left
-   Click the 'Manage private apps' link at the bottom of the right column, displayed under the app listing
-   Click the name of the private app
-   Copy and paste the password from the 'Admin API' section into the relevant part of the config.yml

## 🛠 Front-End Stack

### HTML/Templating

Shopify uses [Liquid](https://help.shopify.com/en/themes/liquid) for theme templates. Liquid is a fairly straightforward language to use, but sometimes can be verbose compared to other languages. It is written in Ruby and is similar to Python's Jinja templating language.

### JavaScript

-   Compiled with Webpack and Babel
-   ES6 Modules
-   ES6 features (const, let, spread, rest, arrow functions, template literals)
-   [Flow static typing](https://flow.org/)
-   Preact
-   Shopify API

### CSS

-   Compiled with Webpack and NodeSass
-   [Inuit CSS](https://github.com/inuitcss/inuitcss)
-   [Namespacing](https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/)
-   PostCSS
    -   [Autoprefixer](https://www.npmjs.com/package/autoprefixer)
    -   [MQPacker](https://www.npmjs.com/package/css-mqpacker)

We use a combination of BEM and functional CSS (a la [Tachyons](https://github.com/tachyons-css/tachyons)) for building reusable, composable CSS. Where there is feature-specific styling we will create modules that follow BEM principles, but otherwise we use the functional CSS part. E.g. specific classes for flex, padding, margin, background colours etc. The prefixing/namespacing (`c-` for components, `u-` for utility classes, etc.) feature heavily throughout the code.

## 🛠 Back-End Stack

We have a simple Node API for creating products and pages for each store. This can be found in `./api/scaffold-content.js`

## 💻 Developing

To work on Shopify themes locally you must have Shopify's [Slate](https://github.com/Shopify/slate) tools installed. This is a powerful CLI that includes testing and deployment to your Shopify store and it is _very_ helpful.

For all the required information on Slate, please [refer to the official documentation](https://shopify.github.io/slate/).

```
⚠️ Slate is currently being upgraded to version 4, which is a rewrite of the tool.
This update brings some much needed improvements, but it's also more difficult to
integrate with our custom Webpack scripts.

The installed version of Slate should remain at version 3 until this is fixed. This
also means Node versions should be v8.X.X major, we haven't tested versions 9 or 10,
but we know 11 does not work with Slate v3.
```

### Linting

The project uses [Flow](https://flow.org/) and [Prettier](https://github.com/prettier/prettier). These are tools that can be used from the command line and/or as part of a build system, but work even better when use with your favourite editor (i.e. Sublime) as they format and validate your code as you type/save.

These instructions are for Sublime, but there are alternatives available for other editors or IDEs too.

-   [http://www.sublimelinter.com/en/stable/](http://www.sublimelinter.com/en/stable/)
-   [https://github.com/SublimeLinter/SublimeLinter-flow](https://github.com/SublimeLinter/SublimeLinter-flow)
-   [https://github.com/jonlabelle/SublimeJsPrettier](https://github.com/jonlabelle/SublimeJsPrettier)

```
⚠️ It's worth noting that we currently handle type errors as warnings/suggestions,
they will not break the build, but they should be fixed.
```

### Running

We don't use Slate's JS or Sass tools so we need to run both our Webpack tools and Slate's tools together. Instead of the standard slate start command, we need to instead run:

`yarn dev:[shop name]`

This will start the Shopify content syncing and Webpack will begin watching for changes.

```
⚠️ When developing locally, any changes are automatically synced with the
Shopify store, so it's important to only work on the staging store in case any
breaking changes are introduced to the production store.
```

## 🚀 Deploying code

### Shopify theme

To build a production-ready version of the theme you need to run a single command which will then compile and minify all your JavaScript and Sass and then deploy your code to the defined store environment. For example:

`yarn deploy:[shop name]`

All assets are then synced with Shopify's CDN.

## 🗃 Content Management

Content is edited in 2 places on a Shopify store. The main built-in content types that are edited from the Shopify admin are Collections, Products, Blogs and Pages.

Any additional content is edited via the Theme Editor (accessible via Online Store section of the admin). Custom content is created via [Sections](https://help.shopify.com/en/themes/development/sections), with each section have its own content model.

### Access credentials

User accounts are available via LastPass in the shared project folder.

## 🌟 Notable features

```
⚠️ All store settings can be configured in the 'Theme settings' section of the theme editor.
```

### Multi-store

Both brand's have multiple stores and currencies.

Upon landing on a store, we check the user's location and match that against the country of the store. If, for example, the user is visiting the US store but is in the UK, we will prompt the user to confirm which store they would like to use.

Currencies are converted and displayed inline. However it's worth noting that when a user checks out they can only checkout in the single currency that the store uses.

The user can edit their store and currency preference at any time from the site nav bar.

#### Smith & Sinclair

Stores are:

-   UK
-   USA
-   France
-   Spain
-   Italy
-   Germany
-   The Netherlands

#### Pollen

TBD.

### Age Restrictions

Both stores apply some sort of age restriction on content. We cannot have 100% control over what is added to the cart due to the inability to have access to Shopify's APIs and built-in endpoints. However, we can apply some rules that can help verify that users have confirmed their age.

Any product that has age restrictions must have the _age_check_ tag. This is how we know whether to restrict a product or not. For products that have this tag, we disable form submissions and add the age confirmation modal before purchase. It's only after we confirm a user's location (via IP checking service) age and , where applicable, their state, that we then enable adding a product to the cart (this is handled via the built-in AJAX API).

After a confirmed age-restricted product is added to the cart we add a cart note to confirm this. This can be used in verify the order in the admin.

A final, "just in case" validation check is done when checking out. We check all the products in the cart to see if they have restrictions. If we find restricted products but there hasn't been user confirmation we simply remove these products before continuing to the checkout.

#### Smith & Sinclair

In the UK, we have to ensure users are 18 or older if they purchase products containing alcohol. In the US, we have to ensure users 21 or older and also validate their State to comply with shipping laws.

![Age restrictions](smith&sinclair-age-restrictions.png)

#### Pollen

TBD.

## 🏗 Store setup

The following are requirements for each store.

- Scaffold content
- Set up navigation for header, footer, overlay and social
- Update Theme Settings to ensure correct settings for each country's store
- Ensure age-restricted products are tagged correctly



## 👋 Contributing

If you are adding a new feature or fix to the project please follow these steps:

-   Checkout master and pull all latest changes
-   Create a new branch (`git checkout -b (fix|feature)/[ticket number]_[branch name]`). For example: `git checkout -b fix/MD-123_timeout-error`.
-   Do your typing and write your code
-   When ready to go, create a Pull Request to Staging detailing the changes made with an updated Changelog and Readme if required
-   Once code reviewed, deploy changes to the Staging environment and test there
-   Once approved, create a Pull Request to Master
-   Once code reviewed, deploy changes to Production environment
-   Smoke test changes on Production environment
-   Done!

## 🤓 Further reading

Here is some helpful links for important Shopify theme development features:

-   [Theme structure](https://help.shopify.com/en/themes/development/templates) - This outlines the important architecture of templates, sections and snippets.
-   [Theme sections](https://help.shopify.com/en/themes/development/sections) - This is how create content-managed 'sections' for our pages
-   [Navigation](https://help.shopify.com/en/themes/development/building-nested-navigation) - We use a few single and nested navigations around the site
-   [Liduid template basics](https://help.shopify.com/en/themes/liquid/basics) - Basic need-to-know for using Liquid templates
-   [Liquid objects](https://help.shopify.com/en/themes/liquid/objects) - Liquid objects contain all the information needed to get data from your store to your HTML (e.g. product information, settings)
-   [Liquid tags](https://help.shopify.com/en/themes/liquid/tags) - These are important for building your templates, including control flow and iteration
-   [Liquid filters](https://help.shopify.com/en/themes/liquid/filters) - Filters are used to manipulate the information stored in objects
-   [Slate](https://shopify.github.io/slate/) - This is the CLI tool we use for syncing content and files between our local development environment and a live store

## 💬 Discussion

You can join the project's [Slack channel](https://maidoteam.slack.com/messages/CJ59JA9PU) for any discussion.
