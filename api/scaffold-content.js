const yaml = require('js-yaml');
const fs = require('fs');
const Shopify = require('shopify-api-node');
const {argv} = require('yargs');
const find = require('lodash/find');
const kebabCase = require('lodash/kebabCase');
const map = require('lodash/map');
const startCase = require('lodash/startCase');
const Listr = require('listr');
const Table = require('cli-table3');
const contentToCreate = require('./content');

//

const allShopifyConfig = yaml.safeLoad(fs.readFileSync('./config.yml', 'utf8'));

if (!allShopifyConfig) {
    return console.error('Error reading the .yml file');
}

if (!argv.e) {
    return console.error('No environment specified');
}

const shopifyConfig = allShopifyConfig[argv.e];

if (!shopifyConfig) {
    return console.error('No config defined for environment');
}

//

const shopify = new Shopify({
    shopName: shopifyConfig.store,
    apiKey: shopifyConfig.api_key,
    password: shopifyConfig.password
});

const createPages = ctx => {
    return new Promise(async (resolve, reject) => {
        const existingPages = await shopify.page.list();
        const skippedPages = [];

        const pages = contentToCreate.pages
            .filter(page => {
                const existingPage = find(existingPages, {title: page.title});

                if (existingPage) {
                    skippedPages.push(page.title);
                    return false;
                }

                return true;
            })
            .map(page => shopify.page.create(page));

        Promise.all(pages)
            .then(response => {
                ctx.pages = {
                    skipped: skippedPages,
                    created: response.map(page => page.title)
                };

                resolve();
            })
            .catch(reject);
    });
};

const createProducts = ctx => {
    return new Promise(async (resolve, reject) => {
        const existingProducts = await shopify.product.list();
        const skippedProducts = [];

        const products = contentToCreate.products
            .filter(product => {
                const existingProduct = find(existingProducts, {title: product.title});

                if (existingProduct) {
                    skippedProducts.push(product.title);
                    return false;
                }

                return true;
            })
            .map(product => {
                return new Promise(resolve2 => {
                    shopify.product.create(product).then(newProduct => {
                        shopify.productVariant
                            .update(newProduct.variants[0].id, {
                                price: product.price,
                                title: product.title,
                                inventory_quantity: 100
                            })
                            .then(resolve2);
                    });
                });
            });

        Promise.all(products)
            .then(response => {
                ctx.products = {
                    skipped: skippedProducts,
                    created: response.map(product => product.title)
                };

                resolve();
            })
            .catch(reject);
    });
};

const createCollections = ctx => {
    return new Promise(async (resolve, reject) => {
        const existingCollections = await shopify.smartCollection.list();
        const skippedCollections = [];

        const collections = contentToCreate.collections
            .filter(collection => {
                const existingCollection = find(existingCollections, {title: collection.title});

                if (existingCollection) {
                    skippedCollections.push(collection.title);
                    return false;
                }

                return true;
            })
            .map(collection => {
                return {
                    ...collection,
                    image: {
                        attachment: fs.readFileSync(
                            `./api/images/${kebabCase(collection.title)}.png`,
                            'base64'
                        )
                    }
                };
            })
            .map(collection => shopify.smartCollection.create(collection));

        Promise.all(collections)
            .then(response => {
                ctx.collections = {
                    skipped: skippedCollections,
                    created: response.map(collection => collection.title)
                };

                resolve();
            })
            .catch(reject);
    });
};

const init = () => {
    const tasks = new Listr([
        {
            title: 'Connecting to Shopify',
            task: () => Promise.resolve('connected')
        },
        {
            title: 'Create pages',
            task: ctx => createPages(ctx)
        },
        {
            title: 'Create products',
            task: ctx => createProducts(ctx)
        },
        {
            title: 'Create collections',
            task: ctx => createCollections(ctx)
        }
    ]);

    tasks
        .run()
        .then(ctx => {
            const content = [
                ['pages', 'Pages'],
                ['products', 'Products'],
                ['collections', 'Collections']
            ];

            content.forEach(c => {
                if (ctx[c[0]]) {
                    const table = new Table();

                    table.push([{colSpan: 2, content: c[1]}]);

                    map(ctx[c[0]], (value, key) => {
                        table.push([startCase(key), value.join('\n')]);
                    });

                    console.log(table.toString());
                }
            });
        })
        .catch(console.log);
};

init();
