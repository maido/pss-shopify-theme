const $items = document.querySelectorAll('.product-list > [data-category]');
const products = [];

if ($items) {
    [...$items].map($item => {
        const title = $item.querySelector('.card-text').innerText;
        const price = $item.querySelector('.price')
            ? parseInt($item.querySelector('.price').innerText.replace('£', ''))
            : 0;
        const image = `https://www.smithandsinclair.com${$item
            .querySelector('.card-img-top')
            .getAttribute('src')}`;
        let collections = $item.dataset.category;

        const product_type = collections
            .split(',')
            .filter(c => c !== '' && c !== 'All Products' && c !== 'Best Sellers')[0];

        products.push({title, images: [{src: image}], price, product_type, published: true});
    });
}

console.log(products);
