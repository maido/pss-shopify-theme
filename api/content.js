const pages = [
    {
        title: 'About',
        template_suffix: 'about',
        published: true
    },
    {
        title: 'Experiences',
        template_suffix: 'experiences-list',
        published: true
    },
    {
        title: 'Experiences - The Gin Experience',
        handle: 'experiences/corporate/the-gin-experience',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Garnish Bar',
        handle: 'experiences/corporate/garnish-bar',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Edible Bubbles',
        handle: 'experiences/corporate/edible-bubbles',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Vapour Orb',
        handle: 'experiences/corporate/vapour-orb',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Candy Tray',
        handle: 'experiences/corporate/candy-tray',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Dipper Station',
        handle: 'experiences/corporate/dipper-station',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - The Sherbet Wall',
        handle: 'experiences/corporate/the-sherbet-wall',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Ballon Bursst',
        handle: 'experiences/corporate/balloon-burst',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Masterclasses',
        handle: 'experiences/corporate/masterclasses',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - The Flavour Gallery',
        handle: 'experiences/pop-ups/the-flavour-gallery',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Tanqueray',
        handle: 'experiences/pop-ups/tanqueray',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Sanderson',
        handle: 'experiences/pop-ups/sanderson',
        template_suffix: 'experiences-detail',
        published: true
    },
    {
        title: 'Experiences - Blue Monday with Bumble',
        handle: 'experiences/pop-ups/blue-monday-with-bumble',
        template_suffix: 'experiences-detail',
        published: true
    },
    // site furniture
    {
        title: 'Contact Us',
        template_suffix: 'contact',
        published: true
    },
    {
        title: 'Delivery and Returns',
        template_suffix: 'refund-policy',
        published: true
    },
    {
        title: 'Privacy Policy',
        template_suffix: 'privacy-policy',
        published: true
    },
    {
        title: 'Cookie Policy',
        template_suffix: 'cookie-policy',
        published: true
    },
    {
        title: 'Terms and Conditions',
        template_suffix: 'terms-and-conditions',
        published: true
    },
    {
        title: 'FAQs',
        template_suffix: 'faqs',
        published: true
    }
];

const collections = [
    {
        title: 'All Products',
        sort_order: 'alpha-asc',
        rules: [{column: 'title', relation: 'not_contains', condition: '******'}]
    },
    {
        title: 'Best Sellers',
        sort_order: 'best-selling',
        rules: [{column: 'title', relation: 'not_contains', condition: '******'}]
    },
    {
        title: 'Gift Sets',
        sort_order: 'alpha-asc',
        rules: [{column: 'type', relation: 'equals', condition: 'Gift Set'}]
    },
    {
        title: 'Alcoholic Cocktail Gummies',
        sort_order: 'alpha-asc',
        rules: [{column: 'type', relation: 'equals', condition: 'Alcoholic Cocktail Gummies'}]
    },
    {
        title: 'Edible Fragnance',
        sort_order: 'alpha-asc',
        rules: [{column: 'type', relation: 'equals', condition: 'Edible Fragnance'}]
    },
    {
        title: 'Cocktail F.I.Z.Z.',
        sort_order: 'alpha-asc',
        rules: [{column: 'type', relation: 'equals', condition: 'Cocktail F.I.Z.Z.'}]
    },
    {
        title: 'Alcoholic Cocktail Dippers',
        sort_order: 'alpha-asc',
        rules: [{column: 'type', relation: 'equals', condition: 'Alcoholic Cocktail Dipper'}]
    },
    {
        title: 'The Cocktail Picnic Hamper',
        sort_order: 'alpha-asc',
        rules: [{column: 'type', relation: 'equals', condition: 'The Cocktail Picnic Hamper'}]
    }
];

const products = [
    {
        title: 'The After Dinner Selection',
        images: [{src: 'https://www.smithandsinclair.com/media/1266/after1.png'}],
        price: 15,
        product_type: 'Alcoholic Cocktail Gummies',
        published: true
    },
    {
        title: 'The Celebration Selection',
        images: [{src: 'https://www.smithandsinclair.com/media/1264/celebration1.png'}],
        price: 15,
        product_type: 'Alcoholic Cocktail Gummies',
        published: true
    },
    {
        title: "The 'For You' Selection",
        images: [{src: 'https://www.smithandsinclair.com/media/1577/foryou.png'}],
        price: 15,
        product_type: 'Alcoholic Cocktail Gummies',
        published: true
    },
    {
        title: 'The Gin Obsessed Selection',
        images: [{src: 'https://www.smithandsinclair.com/media/1265/gin1.png'}],
        price: 15,
        product_type: 'Alcoholic Cocktail Gummies',
        published: true
    },
    {
        title: 'The Night in Selection',
        images: [{src: 'https://www.smithandsinclair.com/media/1267/night-in1.png'}],
        price: 15,
        product_type: 'Alcoholic Cocktail Gummies',
        published: true
    },
    {
        title: 'The Party Selection',
        images: [{src: 'https://www.smithandsinclair.com/media/1268/party1.png'}],
        price: 15,
        product_type: 'Alcoholic Cocktail Gummies',
        published: true
    },
    {
        title: 'Cherry Blossom & Mandarin',
        images: [{src: 'https://www.smithandsinclair.com/media/1141/productnew.png'}],
        price: 10,
        product_type: 'Edible Fragrance',
        published: true
    },
    {
        title: 'Cocktail F.I.Z.Z. Set',
        images: [{src: 'https://www.smithandsinclair.com/media/1570/image.png'}],
        price: 15,
        product_type: 'Gift Sets',
        published: true
    },
    {
        title: 'Edible Fragrance Set',
        images: [{src: 'https://www.smithandsinclair.com/media/1358/three-fragrances.png'}],
        price: 25,
        product_type: 'Edible Fragrance',
        published: true
    },
    {
        title: 'Elderflower Spritz',
        images: [{src: 'https://www.smithandsinclair.com/media/1322/fyf11.png'}],
        price: 6,
        product_type: 'Cocktail F.I.Z.Z.',
        published: true
    },
    {
        title: 'Peach Bellini',
        images: [{src: 'https://www.smithandsinclair.com/media/1555/blogspritx1.png'}],
        price: 6,
        product_type: 'Cocktail F.I.Z.Z.',
        published: true
    },
    {
        title: 'Pear & Vanilla',
        images: [{src: 'https://www.smithandsinclair.com/media/1105/product-new123456.png'}],
        price: 10,
        product_type: 'Edible Fragrance',
        published: true
    },
    {
        title: 'Rhubarb Mimosa',
        images: [{src: 'https://www.smithandsinclair.com/media/1312/fyf6.png'}],
        price: 6,
        product_type: 'Cocktail F.I.Z.Z.',
        published: true
    },
    {
        title: 'The Cocktail Picnic Hamper',
        images: [{src: 'https://www.smithandsinclair.com/media/1574/thaaaaaaaaaai.png'}],
        price: 45,
        product_type: 'The Cocktail Picnic Hamper',
        published: true
    },
    {
        title: 'The Double Dipper',
        images: [{src: 'https://www.smithandsinclair.com/media/1463/doubledip.png'}],
        price: 0,
        product_type: 'Alcoholic Cocktail Dipper',
        published: true
    },
    {
        title: 'The Gincredible Dipper',
        images: [{src: 'https://www.smithandsinclair.com/media/1498/img.png'}],
        price: 6,
        product_type: 'Alcoholic Cocktail Dipper',
        published: true
    },
    {
        title: 'Tropical Passion Fruit',
        images: [{src: 'https://www.smithandsinclair.com/media/1316/fyf10.png'}],
        price: 6,
        product_type: 'Cocktail F.I.Z.Z.',
        published: true
    },
    {
        title: 'Watermelon & Citrus',
        images: [{src: 'https://www.smithandsinclair.com/media/1112/xproduct-new23.png'}],
        price: 10,
        product_type: 'Edible Fragrance',
        published: true
    },
    {
        title: 'Love Sticker Set',
        images: [{src: 'https://www.smithandsinclair.com/media/1482/stickerpack.png'}],
        price: 1,
        published: true
    }
];

module.exports = {
    pages,
    collections,
    products
};
